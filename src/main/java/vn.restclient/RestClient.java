package vn.restclient;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Copyright © 2016 Neo-Lab Co.,Ltd.
 * Created by Hieu➈ on 31/10/2016.
 */

public class RestClient {

    private Retrofit mRetrofit;

    private OkHttpClient.Builder mOkHttpClient;

    private static RestClient mRestClient;

    /**
     * Create instance Rest Client
     * */
    public static RestClient getInstance(){
        if(mRestClient ==null) mRestClient = new RestClient();
        return mRestClient;
    }

    /**
     * Use Retrofit for Rest Client
     * */
    public  Retrofit getRetrofit(String pUrl) {

        if(mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(pUrl)
                    .client(getOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return mRetrofit;
    }

    /**
     * Use OkHttpClient3 for Rest Client
     * */
    private  OkHttpClient getOkHttpClient(){

        if(mOkHttpClient == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);


            mOkHttpClient = new OkHttpClient.Builder();
            mOkHttpClient.addInterceptor(logging);
            mOkHttpClient.interceptors().add(chain -> {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder();
                Request request = requestBuilder.build();
                return chain.proceed(request);
            });

        }

        return mOkHttpClient.build();
    }

    /**
     * Destroy Rest Client
     * */
    public void destroy() {
        if (mOkHttpClient != null) mOkHttpClient = null;
        if (mRetrofit != null) mRetrofit = null;
        if(mRestClient != null) mRestClient = null;
    }

}
